
#include <fstream>
#include <math.h>
#include <stdlib.h>
#include <time.h>

using namespace std;

int main()
{
    int line_count = 10, coord_count = 4;
    float coord_max = 10, coord_min = -10, coord_size = coord_max - coord_min;
    srand(time(NULL));
    ofstream data;
    data.open("1_13_data.txt");
    for(int i = 0; i < line_count; ++i)
    {
            data << fmod(float(rand()), coord_size) + coord_min;
            for(int j = 0; j < coord_count - 1; ++j)
            {
                    data << ' ';
                    data << fmod(float(rand()), coord_size) + coord_min;
            }
            data << '\n';
    }
    data.close();
    return 0;
}
