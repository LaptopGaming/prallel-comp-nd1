#include<fstream>
#include<string>
#include<iostream>
#include<vector>
#include<algorithm>
using namespace std;

int main()
{
    //------------------------------------------------------------ word splitting
    ifstream data("2_8_data.txt");
    vector<char> delim = {'?', '.', ',', ':', ';', '!'};
    vector<string> words = {};
    do
    {
        string temp_str;
        vector<int> delim_pos = {};
        data >> temp_str;
        for(int i = 0; i < delim.size(); ++i)
        {
            int del_pos = -1;
            while(true)
            {
                del_pos = temp_str.find(delim[i], del_pos+1);
                if(del_pos != -1)
                    delim_pos.push_back(del_pos);
                else break;
            }
        }
        delim_pos.push_back(-1);
        sort(delim_pos.begin(), delim_pos.end());
        delim_pos.push_back(temp_str.size());
        for(int i = 1; i < delim_pos.size(); ++i)
        {
            if(delim_pos[i] - delim_pos[i-1] < 2)
                continue;
            words.push_back(temp_str.substr(delim_pos[i-1] + 1, delim_pos[i] - delim_pos[i-1]-1));
        }
    }while(!(data.eof())); // could be simplified by putting the entire file into the temp_str but that may cause memory issues on larger files
    /*
    for(int i = 0; i < words.size(); ++i)
        cout << words[i] << ' ';
    cout << "\n\n";
    */
    //----------------------------------------------------------------- Ex. operations
    char mid_lett = words[words.size()/2][(words[words.size()/2]).size()/2];
    int match = 0;
    for(int i = 0; i < words.size(); ++i)
    {
        if(words[i].find(mid_lett) != string::npos)
            ++match;
    }
    cout << "Ats: " << match;
    return 0;
}
