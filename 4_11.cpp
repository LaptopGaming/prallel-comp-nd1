#include<iostream>
#include<math.h>
#include<fstream>

using namespace std;

float user_funct(float x);
float root_calc(float (*funct)(float), float x_0, float x_1, float accuracy, bool show_iter_val = false);

int main()
{
    cout << "Enter starting points x0 and x1\n";
    float x0, x1;
    cin >> x0 >> x1;
    cout << "Starting points " << x0 << " and " << x1 << " chosen\n";
    cout << "Enter desired accuracy\n";
    float acc;
    cin >> acc;
    cout << "Accuracy of " << acc << " chosen\n";
    cout << "Do you want to see the iterations? (y/n)\n";
    char choice;
    cin >> choice;
    bool show_iter;
    if(choice == 'y')
    {
        show_iter = true;
        cout << "Iterations will be shown\n";
    }
    else
    {
        show_iter = false;
        cout << "Iterations will not be shown\n";
    }

    float answ = root_calc(user_funct, x0, x1, acc);
    /*
    string input_func = "pow(x, 2) - 4;";
    if(input_func[input_func.size() - 1] != ';')
        input_func.push_back(';');
    ofstream ufunc("4_11_ufunc.cpp");
    ufunc << "#include<math.h>\n#include \"4_11_ufunc.h\"\nusing namespace std;\n\nint user_funct(float x)\n{\n\treturn " << input_func << "\n}\n"; //this is very unsafe...
    ufunc.close();
    #include"4_11_ufunc.h"
    */
    //float answ = root_calc(user_funct, 0, rand() % 10, 0.001);
    cout << "Answer: " << answ;
    return 0;
}


float user_funct(float x)
{
    return pow(x, 2) - 4;
}


float root_calc(float (*funct)(float), float x_0, float x_1, float accuracy, bool show_iter_val)
{
    float x_2 = x_1 - funct(x_1)*(x_1 - x_0)/(funct(x_1) - funct(x_0));
    if(show_iter_val)
        cout << x_2 << '\n';
    if(abs(x_2 - x_1) > accuracy)
        return root_calc(funct, x_1, x_2, accuracy, true);
    return x_2;
}
