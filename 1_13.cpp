#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <math.h>
using namespace std;

float Qsum(vector<float> x1, vector<float> x2, vector<float> y1, vector<float> y2, int Q_num);
float Qsum_mult(vector<float> x1, vector<float> x2, vector<float> y1, vector<float> y2, vector<int> Q_vec);
vector<int> axis_cross(vector<float> x1, vector<float> x2, vector<float> y1, vector<float> y2);
struct line
{
    int line_num;
    float line_len;
};
line max_len(vector<float> x1, vector<float> x2, vector<float> y1, vector<float> y2);

int main()
{
    ifstream data("1_13_data.txt");
    int line_count;
    string dummystr;

    while(getline(data, dummystr))
        ++line_count;

    data.clear();
    data.seekg(0);
    vector<float> x1(line_count);
    vector<float> y1(line_count);
    vector<float> x2(line_count);
    vector<float> y2(line_count);

    for(int i = 0; i < line_count; ++i)
    {
        data >> x1[i];
        data >> y1[i];
        data >> x2[i];
        data >> y2[i];
    }
    //cout << x1[0] << ' ' << y1[0] << ' ' << x2[0] << ' ' << y2[0] << ' ';

    //---------------------------------------------------------------------------

    cout << "Ex 1.13 a) \n" << Qsum(x1, x2, y1, y2, 1) << "\n\n"; // objective (a)

    vector<int> Q_vec = {1, 3};
    cout << "Ex 1.13 b) \n" << Qsum_mult(x1, x2, y1, y2, Q_vec) << "\n\n"; // objective (b)

    cout << "Ex 1.13 c) \n" << "Line numbers, that cross the axis:" << ' ';
    vector<int> croserrs = axis_cross(x1, x2, y1, y2);
    cout << croserrs[0];
    for(int i = 1; i < croserrs.size(); ++i)                // objective (c)
        cout << ", " << croserrs[i];
    cout << '\n';

    line max_line = max_len(x1, x2, y1, y2);
    cout << "Ex 1.13 d) \n" << "Line number: " << max_line.line_num << "\nLine length: " << max_line.line_len << '\n'; //objective (d)
    return 0;
}

float Qsum(vector<float> x1, vector<float> x2, vector<float> y1, vector<float> y2, int Q_num)
{
    //if((x1.size() != x2.size()) || (y1.size() != y2.size()) || (x1.size() != y1.size()))
    //if((Q_num > 4) || Q_num < 0)
    int Qlim_low = int(x1.size() / 4) * (Q_num - 1), Qlim_high = int(x1.size() / 4) * Q_num;
    float len_sum = 0;
    for(int i = Qlim_low; i < Qlim_high; ++i)
        len_sum += sqrt(pow(x1[i] - x2[i], 2) + pow(y1[i] - y2[i], 2));
    return len_sum;
}

float Qsum_mult(vector<float> x1, vector<float> x2, vector<float> y1, vector<float> y2, vector<int> Q_vec)
{
    float fin_sum = 0;
    for(int i = 0; i < Q_vec.size(); ++i)
        fin_sum += Qsum(x1, x2, y1, y2, Q_vec[i]);
    return fin_sum;
}

vector<int> axis_cross(vector<float> x1, vector<float> x2, vector<float> y1, vector<float> y2)
{

    vector<bool> axcross(x1.size());
    int cross_count = 0;
    for(int i = 0; i < x1.size(); ++i)
        if(((x1[i] < 0) && (x2[i] > 0)) || ((x1[i] > 0) && (x2[i] < 0)) || ((y1[i] < 0) && (y2[i] > 0)) || ((y1[i] < 0) && (y2[i] > 0)))
        {
            axcross[i] = true;
            ++cross_count;
        } else axcross[i] = false;
    vector<int> result(cross_count);
    int j = 0;
    for(int i = 0; i < x1.size(); ++i)
        if(axcross[i])
        {
            result[j] = i;
            ++j;
        }
    return result;
}

line max_len(vector<float> x1, vector<float> x2, vector<float> y1, vector<float> y2)
{
    line max_line;
    max_line.line_len = 0;
    for(int i = 0; i < x1.size(); ++i)
    {
        float temp_size = sqrt(pow(x1[i] - x2[i], 2) + pow(y1[i] - y2[i], 2));
        if(max_line.line_len < temp_size)
        {
            max_line.line_len = temp_size;
            max_line.line_num = i;
        }
    }
    return max_line;
}
