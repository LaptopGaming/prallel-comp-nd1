#include<vector>
#include<fstream>
#include<string>
#include<iostream>
#include<algorithm>
using namespace std;

struct stud
{
    string pavarde;
    float vidurkis;
    string grupes_pavadinimas;
};

bool stud_cmp(stud student1, stud student2);

int main()
{
    ifstream data("3_3_data.txt");

    /*
    string dummystr;
    int line_count = 0;
    while(getline(data, dummystr))
        ++line_count;
    data.clear();
    data.seekg(0);

    vector<stud> students(line_count);

    for(int i = 0; i < line_count; ++i)
    {
        data >> students[i].pavarde;
        data >> students[i].vidurkis;
        data >> students[i].grupes_pavadinimas;
    }
    */

    vector<stud> stud_filt = {};
    while(!(data.eof()))
    {
        stud temp;
        data >> temp.pavarde;
        data >> temp.vidurkis;
        data >> temp.grupes_pavadinimas;
        if((temp.vidurkis >= 8) && (temp.grupes_pavadinimas[0] == 'F') && (temp.grupes_pavadinimas[1] == 'F'))
            stud_filt.push_back(temp);
    }


    //sort(stud_filt.begin(), stud_filt.end(), stud_cmp);


    int j = stud_filt.size()/2;
    int k = 0;
    vector<stud> stud_sort(stud_filt.size());
    sort(stud_filt.begin(), stud_filt.begin() + stud_filt.size()/2, stud_cmp);
    sort(stud_filt.begin() + stud_filt.size()/2, stud_filt.end() , stud_cmp);
    for(int i = 0; i < stud_filt.size()/2; ++i)
    {
        while(!(stud_cmp(stud_filt[i], stud_filt[j])))
        {
            if(j>stud_filt.size())
                break;
            stud_sort[k] = stud_filt[j];
            ++j;
            ++k;
        }
        stud_sort[k] = stud_filt[i];
        ++k;
    }
    for(int i = j; i < stud_filt.size(); ++i)
    {
        stud_sort[k] = stud_filt[i];
        ++k;
    }

    for(int i = 0; i < stud_filt.size(); ++i)
    {
        cout << stud_sort[i].pavarde << ' ';
        cout << stud_sort[i].vidurkis << ' ';
        cout << stud_sort[i].grupes_pavadinimas << '\n';
    }
    return 0;
}

bool stud_cmp(stud student1, stud student2)
{
    bool answ;
    if(student1.vidurkis != student2.vidurkis)
        return (student1.vidurkis < student2.vidurkis);
    int scnd_cmp = (student1.grupes_pavadinimas).compare(student2.grupes_pavadinimas);
    if(scnd_cmp != 0)
        return (scnd_cmp < 0);
    int third_cmp = (student1.pavarde).compare(student2.pavarde);
    return (third_cmp < 0);
}
